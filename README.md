# 手写Tomcat框架

#### 介绍
Tomcat，这只3脚猫，大学的时候就认识了，直到现在工作中，也常会和它打交道。这是一只神奇的猫，今天让我来抽象你，实现你！

#### 软件架构
软件架构说明

Tomcat是非常流行的Web Server，它还是一个满足Servlet规范的容器。那么想一想，Tomcat和我们的Web应用是什么关系？

从感性上来说，我们一般需要把Web应用打成WAR包部署到Tomcat中，在我们的Web应用中，我们要指明URL被哪个类的哪个方法所处理（不论是原始的Servlet开发，还是现在流行的Spring MVC都必须指明）。

由于我们的Web应用是运行在Tomcat中，那么显然，请求必定是先到达Tomcat的。Tomcat对于请求实际上会进行下面的处理：

第一：提供Socket服务

Tomcat的启动，必然是Socket服务，只不过它支持HTTP协议而已！

这里其实可以扩展思考下，Tomcat既然是基于Socket，那么是基于BIO or NIO or AIO呢？

第二：进行请求的分发

要知道一个Tomcat可以为多个Web应用提供服务，那么很显然，Tomcat可以把URL下发到不同的Web应用。

第三：需要把请求和响应封装成request/response

我们在Web应用这一层，可从来没有封装过request/response的，我们都是直接使用的，这就是因为Tomcat已经为你做好了！



#### 安装教程

1.  测试类： TestMyTomcat
2.   打印日志：
Connected to the target VM, address: '127.0.0.1:2748', transport: 'socket'
MyTomcat is start...
MyRequest{url='/HelloWorld', method='GET'}
-------doDispatch: request: MyRequest{url='/HelloWorld', method='GET'}
socket.close()
MyRequest{url='/favicon.ico', method='GET'}
-------doDispatch: request: MyRequest{url='/favicon.ico', method='GET'}
socket.close()
MyRequest{url='/', method='GET'}
-------doDispatch: request: MyRequest{url='/', method='GET'}
socket.close()
MyRequest{url='/favicon.ico', method='GET'}
-------doDispatch: request: MyRequest{url='/favicon.ico', method='GET'}
socket.close()
MyRequest{url='/HelloWorld2', method='GET'}
-------doDispatch: request: MyRequest{url='/HelloWorld2', method='GET'}
socket.close()
MyRequest{url='/favicon.ico', method='GET'}
-------doDispatch: request: MyRequest{url='/favicon.ico', method='GET'}
socket.close()
MyRequest{url='/HelloWorld2', method='GET'}
-------doDispatch: request: MyRequest{url='/HelloWorld2', method='GET'}
socket.close()
MyRequest{url='/favicon.ico', method='GET'}
-------doDispatch: request: MyRequest{url='/favicon.ico', method='GET'}
socket.close()
MyRequest{url='/HelloWorld', method='GET'}
-------doDispatch: request: MyRequest{url='/HelloWorld', method='GET'}
socket.close()
MyRequest{url='/favicon.ico', method='GET'}
-------doDispatch: request: MyRequest{url='/favicon.ico', method='GET'}
socket.close()
MyRequest{url='/HelloWorld1', method='GET'}
-------doDispatch: request: MyRequest{url='/HelloWorld1', method='GET'}
socket.close()
MyRequest{url='/favicon.ico', method='GET'}
-------doDispatch: request: MyRequest{url='/favicon.ico', method='GET'}
socket.close()
MyRequest{url='/', method='GET'}
-------doDispatch: request: MyRequest{url='/', method='GET'}
socket.close()
MyRequest{url='/favicon.ico', method='GET'}
-------doDispatch: request: MyRequest{url='/favicon.ico', method='GET'}
socket.close()
MyRequest{url='/', method='GET'}
-------doDispatch: request: MyRequest{url='/', method='GET'}
socket.close()
MyRequest{url='/favicon.ico', method='GET'}
-------doDispatch: request: MyRequest{url='/favicon.ico', method='GET'}
socket.close()
MyRequest{url='/', method='GET'}
-------doDispatch: request: MyRequest{url='/', method='GET'}
socket.close()
MyRequest{url='/favicon.ico', method='GET'}
-------doDispatch: request: MyRequest{url='/favicon.ico', method='GET'}
socket.close()
MyRequest{url='/HelloWorld', method='GET'}
-------doDispatch: request: MyRequest{url='/HelloWorld', method='GET'}
socket.close()
MyRequest{url='/favicon.ico', method='GET'}
-------doDispatch: request: MyRequest{url='/favicon.ico', method='GET'}
socket.close()
MyRequest{url='/StopTheWorld', method='GET'}
-------doDispatch: request: MyRequest{url='/StopTheWorld', method='GET'}
socket.close()
MyRequest{url='/favicon.ico', method='GET'}
-------doDispatch: request: MyRequest{url='/favicon.ico', method='GET'}
socket.close()
MyRequest{url='/StopTheWorld', method='GET'}
-------doDispatch: request: MyRequest{url='/StopTheWorld', method='GET'}
socket.close()
MyRequest{url='/favicon.ico', method='GET'}
-------doDispatch: request: MyRequest{url='/favicon.ico', method='GET'}
socket.close()
MyRequest{url='/StopTheWorld', method='GET'}
-------doDispatch: request: MyRequest{url='/StopTheWorld', method='GET'}
socket.close()
MyRequest{url='/favicon.ico', method='GET'}
-------doDispatch: request: MyRequest{url='/favicon.ico', method='GET'}
socket.close()
MyRequest{url='/StopTheWorld', method='GET'}
-------doDispatch: request: MyRequest{url='/StopTheWorld', method='GET'}
socket.close()
MyRequest{url='/favicon.ico', method='GET'}
-------doDispatch: request: MyRequest{url='/favicon.ico', method='GET'}
socket.close()
MyRequest{url='/StopTheWorld2', method='GET'}
-------doDispatch: request: MyRequest{url='/StopTheWorld2', method='GET'}
socket.close()
MyRequest{url='/favicon.ico', method='GET'}
-------doDispatch: request: MyRequest{url='/favicon.ico', method='GET'}
socket.close()
MyRequest{url='/StopTheWorld', method='GET'}
-------doDispatch: request: MyRequest{url='/StopTheWorld', method='GET'}
socket.close()
MyRequest{url='/favicon.ico', method='GET'}
-------doDispatch: request: MyRequest{url='/favicon.ico', method='GET'}
socket.close()
MyRequest{url='/StopTheWorld', method='GET'}
-------doDispatch: request: MyRequest{url='/StopTheWorld', method='GET'}
socket.close()
MyRequest{url='/favicon.ico', method='GET'}
-------doDispatch: request: MyRequest{url='/favicon.ico', method='GET'}
socket.close()

3.  运行截图：

 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0626/154852_81dc9588_1425724.png "1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0626/154904_c08b0c8b_1425724.png "2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0626/154913_7461506c_1425724.png "3.png")



#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
