package mapping;

import java.util.ArrayList;
import java.util.List;

public class ServletMappingConfig {

    public static List<ServletMapping> servletMappingList = new ArrayList<>();

    static {
        servletMappingList.add(new ServletMapping("HelloWorldServlet","/HelloWorld","servlet.impl.HelloWorldServlet"));
        servletMappingList.add(new ServletMapping("StopTheWorldServlet","/StopTheWorld","servlet.impl.StopTheWorldServlet"));
    }
}
