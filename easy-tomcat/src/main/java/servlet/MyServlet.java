package servlet;

import dto.MyRequest;
import dto.MyResponse;

/**
 *
 * Tomcat是满足Servlet规范的容器，那么自然Tomcat需要提供API。这里你看到了Servlet常见的doGet/doPost/service方法。
 */
public abstract class MyServlet {

    public abstract void doGet(MyRequest request, MyResponse response);

    public abstract void doPost(MyRequest request, MyResponse response);

    public  void service(MyRequest request, MyResponse response){
        String method = request.getMethod().toUpperCase();
        if ("POST".equals(method)){
            doPost(request, response);
        }else if ("GET".equals(method)){
            doGet(request, response);
        }
    }

}
