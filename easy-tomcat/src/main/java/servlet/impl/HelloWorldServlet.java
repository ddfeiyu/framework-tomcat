package servlet.impl;

import dto.MyRequest;
import dto.MyResponse;
import servlet.MyServlet;

import java.io.IOException;

/**
 * 提供这2个具体的Servlet实现，只是为了后续的测试！
 *
 */
public class HelloWorldServlet extends MyServlet {


    @Override
    public void doGet(MyRequest request, MyResponse response) {
        try {
            response.write("get the world");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    @Override
    public void doPost(MyRequest request, MyResponse response) {
        try {
            response.write("post the world");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
